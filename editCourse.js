

let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDescription");
let coursePrice = document.querySelector("#coursePrice");

let token = localStorage.getItem("token")


fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data =>{
	
		courseName.value = (data.name)
		courseDesc.value = (data.description)
		coursePrice.value = (data.price)

		document.querySelector("#editCourse").addEventListener("submit", (e) => {
			e.preventDefault


			//variables representing the values of our inputs
			let name = courseName.value
			let desc = courseDesc.value
			let price = coursePrice.value
		})


		if(courseName === '' && courseDesc === '' && coursePrice === '' ){
			alert("Error! Please fill all the fields")
		}else{

		}fetch(`http://localhost:4000/courses/${courseId}`, {
			method: "PUT",
			headers : {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDesc,
				price: coursePrice
			})
			
		})
		.then(res => res.json())
		.then(data => {
			alert("Course edited succesfully!")
			window.location.replace("./courses.html")
	})	

})
	

	/*
			Activity : Create a PUT request via fetch that will send our new udpated course information to the server and handle the servers response. If the server responds with true (use === true), show and alert that confirms successful course updating, then redirect the user to courses.html

			if not, show an alert with an error message
	*/